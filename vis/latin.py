from __future__ import print_function, unicode_literals

import sys

import ucd

from pyuca import Collator

SC_VALUES = {
    'Arabic': 'Arab',
    'Latin': 'Latn',
    'Hiragana': 'Hira',
    'Vai': 'Vaii',
}


def main():
    script = sys.argv[1]
    latin = ucd.get_codepoints(script)
    assert latin == sorted(latin)
    c = Collator('allkeys.txt')
    latin = sorted(latin, key=lambda cp: c.sort_key(chr(cp)))
    # latin = sorted(latin)

    cp_data = ucd.get_ucd(latin)

    print("""\
    <!DOCTYPE html><html>
    <head><meta charset='utf8'><link rel='stylesheet' href='latin.css'></head>
    <body>""")
    for cp in latin:
        d = cp_data[cp]
        short_na = d['na']
        if short_na.startswith(script.upper()):
            short_na = '⋯' + d['na'][len(script.upper()):]
        if d['gc'][0] == 'L' and short_na.startswith('⋯ LETTER'):
            short_na = '⋯ ⋯' + short_na[len('⋯ LETTER'):]

        print("<span")
        print('  id="{:04X}"'.format(cp))
        print('  data-gc="{}"'.format(d['gc']))
        if d['dt'] != 'None':
            print('  data-dt="{}"'.format(d['dt']))
        print('  data-sc="{}"'.format(SC_VALUES[script]))
        print('  title="U+{:04X} [{}] {}"'.format(cp, d['gc'], short_na))
        print('  >')
        print('  &#x{:x};'.format(cp))
        print('</span>')
    print("</body></html>")


if __name__ == '__main__':
    main()
