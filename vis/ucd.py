from __future__ import print_function, unicode_literals


def get_codepoints(sc):
    cps = []
    with open('Scripts.txt', 'rb') as f:
        for line in f:
            line = line.decode('utf8')
            line = line.split('#', 1)[0]
            if not line.strip():
                continue
            codepoints, script = (f.strip() for f in line.split(';'))
            if script != sc:
                continue
            if '..' in codepoints:
                start, end = (
                    int(cp, 0x10) for cp in codepoints.split('..', 1)
                )
                codepoints = list(range(start, end + 1))
            else:
                codepoints = [int(codepoints, 0x10)]
            cps += codepoints
    return cps


def get_ucd(codepoints):
    ucd = {}
    with open('UnicodeData.txt', 'rb') as f:
        for line in f:
            line = line.decode('utf8')
            line = line.split('#', 1)[0]
            if not line.strip():
                continue
            fields = tuple(f.strip() for f in line.split(';'))
            cp = int(fields[0], 0x10)
            if cp not in codepoints:
                continue
            na = fields[1] or ''
            gc = fields[2] or 'Cn'
            decomp = fields[5]
            dt = 'None'
            dm = [cp]
            if decomp:
                decomp = decomp.split(' ')
                if decomp[0][0] == '<':
                    dt = decomp[0][1:-1]
                    decomp = decomp[1:]
                dm = [int(c, 0x10) for c in decomp]
                if dt == 'None' and dm != [cp]:
                    dt = '_canonical'
            ucd[cp] = {'na': na, 'gc': gc, 'dt': dt, 'dm': dm}
    return ucd
