import ucd

codepoints = ucd.get_codepoints('Latin')
data = ucd.get_ucd(codepoints)
dts = {}
for cp in codepoints:
    dt = data[cp]['dt']
    if not dt in dts:
        dts[dt] = []
    dts[dt].append(cp)

for dt, cps in dts.items():
    print(dt, cps)
