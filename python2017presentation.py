dot_first = 'e\N{COMBINING DOT BELOW}\N{COMBINING CIRCUMFLEX ACCENT}'
dot_first, ' '.join(dot_first)
dot_last = 'e\N{COMBINING CIRCUMFLEX ACCENT}\N{COMBINING DOT BELOW}'
dot_last, ' '.join(dot_last)
dot_first == dot_last

from unicodedata import normalize
dot_first_nfc = normalize('NFC', dot_first)
dot_last_nfc = normalize('NFC', dot_last)
dot_first_nfc == dot_last_nfc

import unicodedata, sys
unicodedata.unidata_version, sys.version

