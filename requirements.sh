#!/bin/sh

# Install pure-python packages.
pip install django pyuca jupyter flake8

# Install headers for any pip packages with C components
# python-dev is only needed if using Python 2, ditto Python 3
#sudo apt install python3-dev python-dev

# ICU version is correct for ubuntu 16.06
#sudo apt install libicu-dev
ICU_VERSION=55.1 pip install pyicu

#sudo apt install libpq-dev
pip install psycopg2

#sudo apt install libmysqlclient-dev
pip install mysqlclient

# install tools used
sudo apt install graphviz fonts-noto
