# -*- encoding: utf-8 -*-

from __future__ import print_function, unicode_literals

from unicodedata import normalize
from functools import partial
from itertools import chain

bases = ('a', 'Å', 'm', 'µ')
DOT = '\N{COMBINING DOT ABOVE}'
def dot(string):
    if string[-1] == DOT:
        return string
    return string + DOT

transforms = (
    ('NFD', partial(normalize, 'NFD')),
    ('NFC', partial(normalize, 'NFC')),
    ('NFKC', partial(normalize, 'NFKC')),
    ('NFKD', partial(normalize, 'NFKD')),
    ('upper', type('').upper),
    ('lower', type('').lower),
    # ('dotted', dot),
)

SAME_LOOK = (
    ('ı',),
    ('i', 'i' + DOT, 'ı' + DOT),
    ('i' + DOT + DOT,),
    ('I',),
    ('I' + DOT, '\u0130'),
    ('I' + DOT + DOT, '\u0130' + DOT),
)

nodes = set(bases)
edges = set()
todo = list(chain(*[
    [(n, t) for t in transforms]
    for n in bases
]))

#print(nodes)

while len(todo):
    node, (trans_name, trans) = todo.pop()
    dest = trans(node)
    if dest not in nodes:
        #print("new node {!r}".format(dest))
        nodes.add(dest)
        todo += [(dest, t) for t in transforms]
    edge = (node, trans_name, dest)
    if edges not in edges:
        #print("new edge {!r}".format(edge))
        edges.add((node, trans_name, dest))
    #print(nodes, edges)

import re
# print a Dot graph
def c(string):
    # replace '_' and non-ASCII with '_' + codepoint in hex.
    result = ''
    for c in string:
        if re.match(r'[a-zA-Z]', c):
            result += c
        else:
            result += '_{:x}'.format(ord(c))
    return result

def x(string):
    return string.encode('ascii', 'xmlcharrefreplace')
            
print('digraph {')
print('\t// nodes')
for n in nodes:
    if c(n) != x(n):
        print('\t{} [label="{} ({})"];'.format(c(n), x(n), c(n)))
    else:
        print('\t{};'.format(c(n)))
print('\n\t// edges')
for s, e, d in edges:
    if s == d:
        continue
    print('\t{} -> {} [label="{}"];'.format(c(s), c(d), x(e)))
print('\n\t// ranks')
#for nodes in SAME_LOOK:
#    graph_name = c(''.join(nodes))
#    print('\tsubgraph cluster{} {{ {} }}'.format(graph_name, ' '.join(c(n) for n in nodes)))
print('}')
