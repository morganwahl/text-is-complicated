# -*- encoding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import print_function

from pyuca import Collator

strings = [
    'noel',
    'nœl',
    'no\u202del\u202c',
    '"noel"',
    'Noel',
    'no\u0308el',
    'noels',
    'nuel',
]

c = Collator()

import struct
import base64

def p(strs):
    output = []
    for s in strs:
        output.append(
            '.'.join([
                base64.b85encode(struct.pack('>H', k)).decode('ascii')
                for k in c.sort_key(s)
            ])
        )
    print('\n'.join(output))
    print()

p(strings)
p(sorted(strings))
p(sorted(strings, key=c.sort_key))
