import MySQLdb

def db_sorter(db, column_charset=None, column_collation=None):
    def sorter(strings):
        c = db.cursor()
        c.execute('create database if not exists text')
        try:
            c.execute('use text')
            coltype = 'string varchar(10)'
            if column_charset:
                coltype += ' character set {}'.format(column_charset)
            if column_collation:
                coltype += ' collate {}'.format(column_collation)
            table_def = '''
                create table if not exists strings (
                    {} not null primary key
                )
            '''.format(coltype)
            # print(table_def)
            c.execute(table_def)
            c.executemany('insert into strings (string) values (%s)', strings)
            c.execute('select string from strings order by string')
            result = [r[0] for r in c.fetchall()]
        finally:
            c.execute('drop database text')
        return result
    return sorter

class Sorter(object):
    def __init__(self, kind, **params):
        if kind == 'python':
            self.sorter = sorted
        if kind == 'mariadb':
            self.sorter = db_sorter(
                MySQLdb.connect(charset='utf8mb4'),
                column_charset='utf8mb4',
                column_collation=params['collation'],
            )

    def sort(self, strings):
        return self.sorter(strings)

outputs = set()

strings = (
    '"punc"',
    '[punc]',
    '(punc)',
    'punc',
    '\u201cpunc\u201d',
)

for sorter_params in (
    {'kind': 'python'},
    {'kind': 'mariadb', 'collation': 'utf8mb4_bin'},
    {'kind': 'mariadb', 'collation': 'utf8mb4_general_ci'},
    {'kind': 'mariadb', 'collation': 'utf8mb4_unicode_ci'},
):
    sorter = Sorter(**sorter_params)
    print(sorter.sort(strings))
